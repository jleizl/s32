let http = require('http')

let courses = [{
        "name": "Civil Engineering",
        "code": "CIV"
    },
    {
        "name": "Mechanical Engineering",
        "code": "MEC"
    },
    {
        "name": "Computer Science and Engineering",
        "code": "CSE"
    }
];

let port = 3000;



const server = http.createServer(function(request, response) {

    if (request.url == '/' && request.method == 'GET') {
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Welcome to Booking System.')

    } else if (request.url == '/profile' && request.method == 'GET') {
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Welcome to your profile!')

    } else if (request.url == '/courses' && request.method == 'GET') {
        console.log(courses)
        response.writeHead(200, { 'Content-Type': 'application/json' })
        response.write(JSON.stringify(courses))
        response.end("Here's our courses available.")

    } else if (request.url == '/addCourse' && request.method == 'POST') {
        let request_body = ' ';
        request.on('data', function(data) {
            request_body += data
            console.log(request_body)
        })
        request.on('end', function() {
            console.log(typeof request_body)
            request_body = JSON.parse(request_body)
            let new_course = {
                "name": request_body.name,
                "description": request_body.description
            }
            console.log(new_course)
            courses.push(new_course)
            console.log(courses)
            response.writeHead(200, { 'Content-Type': 'application/json' })
            response.write(JSON.stringify(new_course))
            response.end()
        })

    } else if (request.url == '/updateCourse' && request.method == 'PUT') {
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end("Update a course to our resources.")

    } else if (request.url == '/archiveCourse' && request.method == 'DELETE') {
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end("Archive courses to our resources.")

    }
})

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);